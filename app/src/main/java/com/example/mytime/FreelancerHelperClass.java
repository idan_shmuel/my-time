package com.example.mytime;

public class FreelancerHelperClass {
    String email, skill, businessName, businessAddress, businessDescription;

    public FreelancerHelperClass() {
    }

    public FreelancerHelperClass(String phoneNumber, String skill, String businessName, String businessAddress, String businessDescription) {
        this.email = phoneNumber;
        this.skill = skill;
        this.businessName = businessName;
        this.businessAddress = businessAddress;
        this.businessDescription = businessDescription;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessDescription() {
        return businessDescription;
    }

    public void setBusinessDescription(String businessDescription) {
        this.businessDescription = businessDescription;
    }
}
