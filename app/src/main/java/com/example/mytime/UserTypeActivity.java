package com.example.mytime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UserTypeActivity extends AppCompatActivity {

    private String email;
    private DatabaseReference reference;
    private String emailWithoutSpecialChars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type);
        //getting the phone number from the previous activity.
        Intent intent = getIntent();
        this.email = intent.getStringExtra("email");
        this.reference = FirebaseDatabase.getInstance().getReference("users");
        this.emailWithoutSpecialChars = this.email.replace("@", "").replace(".", "");
    }

    public void onClientClick(View view) {
        this.reference.child(this.emailWithoutSpecialChars).child("userType").setValue("client");
        Intent intent = new Intent(UserTypeActivity.this, ClientHomeActivity.class);
        intent.putExtra("email", this.email);
        intent.putExtra("userType", "client");
        startActivity(intent);
    }

    public void onFreelancerClick(View view) {
        this.reference.child(this.emailWithoutSpecialChars).child("userType").setValue("freelancer");
        Intent intent = new Intent(UserTypeActivity.this, FreelancerRegisterActivity.class);
        intent.putExtra("email", this.email);
        intent.putExtra("userType", "freelancer");
        startActivity(intent);
    }
}