package com.example.mytime;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class BusinessDetailsActivity extends AppCompatActivity {

    private String businessOwnerEmailWithoutSpecialChars;
    private TextView txtBusinessName;
    private TextView txtBusinessAddress;
    private TextView txtBusinessSkill;
    private TextView txtBusinessDetails;
    private TextView txtBusinessOwner;
    private TextView txtBusinessOwnerPhoneNumber;
    private DatabaseReference usersReference;
    private DatabaseReference freelancersReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_details);
        this.txtBusinessName = findViewById(R.id.txtBusinessName);
        this.txtBusinessAddress = findViewById(R.id.txtBusinessAddress);
        this.txtBusinessSkill = findViewById(R.id.txtBusinessSkill);
        this.txtBusinessDetails = findViewById(R.id.txtBusinessDetails);
        this.txtBusinessOwner = findViewById(R.id.txtBusinessOwner);
        this.txtBusinessOwnerPhoneNumber = findViewById(R.id.txtBusinessOwnerPhoneNumber);
        this.usersReference = FirebaseDatabase.getInstance().getReference("users");
        this.freelancersReference = FirebaseDatabase.getInstance().getReference("freelancers");
        this.businessOwnerEmailWithoutSpecialChars = getIntent().getStringExtra("businessOwnerEmailWithoutSpecialChars");
        setBusinessDetailsFromDB();
    }

    private void setBusinessDetailsFromDB() {
        this.freelancersReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                txtBusinessName.setText(snapshot.child(businessOwnerEmailWithoutSpecialChars).child("businessName").getValue(String.class));
                txtBusinessAddress.setText(snapshot.child(businessOwnerEmailWithoutSpecialChars).child("businessAddress").getValue(String.class));
                txtBusinessSkill.setText(snapshot.child(businessOwnerEmailWithoutSpecialChars).child("skill").getValue(String.class));
                txtBusinessDetails.setText(snapshot.child(businessOwnerEmailWithoutSpecialChars).child("businessDescription").getValue(String.class));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(BusinessDetailsActivity.this, "Fail to get data.", Toast.LENGTH_SHORT).show();
            }
        });

        this.usersReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                txtBusinessOwner.setText("Owner: " + snapshot.child(businessOwnerEmailWithoutSpecialChars).child("username").getValue(String.class));
                txtBusinessOwnerPhoneNumber.setText(snapshot.child(businessOwnerEmailWithoutSpecialChars).child("phoneNumber").getValue(String.class));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(BusinessDetailsActivity.this, "Fail to get data.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onSendMessageClick(View view) {
        Intent intent = new Intent(BusinessDetailsActivity.this, SendSMSActivity.class);
        intent.putExtra("businessOwnerPhoneNumber", this.txtBusinessOwnerPhoneNumber.getText().toString());
        startActivity(intent);
    }

    public void onShowImagesClick(View view) {
        Intent intent = new Intent(BusinessDetailsActivity.this, ShowImagesActivity.class);
        intent.putExtra("emailWithoutSpecialChars", this.businessOwnerEmailWithoutSpecialChars);
        startActivity(intent);
    }
}