package com.example.mytime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Pair;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ClientHomeActivity extends AppCompatActivity {

    private String email;
    private DatabaseReference freelancersReference;
    private Pair[] businessesNameAndEmails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //getting the details from the previous activity.
        Intent intent = getIntent();
        this.email = intent.getStringExtra("email");

        this.freelancersReference = FirebaseDatabase.getInstance().getReference("freelancers");
        setBusinessInTextViews();
    }

    private void setBusinessInTextViews() {
        this.freelancersReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                int i = 0;
                businessesNameAndEmails = new Pair[(int)snapshot.getChildrenCount()];
                for (DataSnapshot ds: snapshot.getChildren()) {
                    businessesNameAndEmails[i] = new Pair(i, ds.getKey());
                    addBusinessLayoutToHomeScreen(ds.child("businessName").getValue(String.class), ds.child("businessAddress").getValue(String.class), i);
                    i++;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(ClientHomeActivity.this, "Fail to get data.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        hideFreelancerOptions(menu);
        return true;
    }

    public void hideFreelancerOptions(Menu menu){
        menu.findItem(R.id.freelancerProfile).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.userProfile) {
            Intent intent = new Intent(ClientHomeActivity.this, UserProfileActivity.class);
            intent.putExtra("email", this.email);
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.logout) {
            createSignOutDialog();
        }
        else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void createSignOutDialog()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Log out of My Time?");
        alertDialog.setPositiveButton("Log Out", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(ClientHomeActivity.this, MainActivity.class));
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) { }
        });
        alertDialog.show();
    }

    private void addBusinessLayoutToHomeScreen(String businessName, String businessDescription, int id) {
        LinearLayout mainLinearLayout = findViewById(R.id.mainLinearLayout);
        LinearLayout newBusinessLinearLayout = new LinearLayout(this);
        newBusinessLinearLayout.setOrientation(LinearLayout.HORIZONTAL);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(1050, 300);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.topMargin = 90;
        Drawable backgroundColor = getApplicationContext().getResources().getDrawable(R.drawable.background_color);
        newBusinessLinearLayout.setBackground(backgroundColor);

        newBusinessLinearLayout.setLayoutParams(params);

        LinearLayout newBusinessDetailsLinearLayout = new LinearLayout(this);
        newBusinessDetailsLinearLayout.setOrientation(LinearLayout.VERTICAL);
        TextView txtBusinessName = new TextView(this);
        TextView txtBusinessDescription = new TextView(this);

        txtBusinessName.setTextColor(Color.parseColor("#FFFFFFFF"));
        txtBusinessName.setTextSize(24);
        txtBusinessName.setPadding(10, 0, 0, 0);
        txtBusinessName.setTypeface(null, Typeface.BOLD);
        txtBusinessDescription.setTextColor(Color.parseColor("#FFFFFFFF"));
        txtBusinessDescription.setTextSize(18);
        txtBusinessDescription.setPadding(10, 0, 0, 0);
        txtBusinessDescription.setTypeface(null, Typeface.BOLD);

        //set the properties for button
        Button moreDetailsBtn = new Button(this);
        moreDetailsBtn.setId(id);
        moreDetailsBtn.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        moreDetailsBtn.setText("More details");
        moreDetailsBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClientHomeActivity.this, BusinessDetailsActivity.class);
                intent.putExtra("businessOwnerEmailWithoutSpecialChars", (String)businessesNameAndEmails[moreDetailsBtn.getId()].second);
                startActivity(intent);
            }
        });
        txtBusinessDescription.setText(businessDescription);
        txtBusinessName.setText(businessName);

        newBusinessDetailsLinearLayout.addView(txtBusinessName);
        newBusinessDetailsLinearLayout.addView(txtBusinessDescription);
        newBusinessLinearLayout.addView(newBusinessDetailsLinearLayout);
        newBusinessLinearLayout.addView(moreDetailsBtn);
        mainLinearLayout.addView(newBusinessLinearLayout);
    }

    @Override
    public void onBackPressed() {
        FirebaseAuth.getInstance().signOut();
        this.finishAffinity();
    }
}