package com.example.mytime;


public interface ProfileInterface {
    void saveUserData(String firstParam, String secondParam);
    void getDataFromDatabase();
    boolean isDataChanged();
}
