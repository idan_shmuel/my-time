package com.example.mytime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private EditText edtTxtUsername;
    private EditText edtTxtEmail;
    private EditText edtTxtPhone;
    private TextInputLayout edtTxtPassword;
    private TextInputLayout edtTxtPasswordAuth;
    private Switch switchAgreeTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        this.firebaseAuth = FirebaseAuth.getInstance();
        this.edtTxtUsername = findViewById(R.id.editTextUsername);
        this.edtTxtEmail = findViewById(R.id.editTextEmailAddress);
        this.edtTxtPhone = findViewById(R.id.editTextPhone);
        this.edtTxtPassword = findViewById(R.id.passwordInputLayout);
        this.edtTxtPasswordAuth = findViewById(R.id.passwordAuthInputLayout);
        this.switchAgreeTerms = (Switch) findViewById(R.id.switchAgreeTerms);
    }

    static boolean isValidEmailAddress(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    public boolean isValidInformation() {
        if (this.edtTxtUsername.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "You did not enter your username.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(this.edtTxtEmail.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "You did not enter your email address.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!isValidEmailAddress(this.edtTxtEmail.getText().toString())) {
            Toast.makeText(getApplicationContext(), "Your email address is invalid.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(this.edtTxtPassword.getEditText().getText().toString().length() < 6) {
            Toast.makeText(getApplicationContext(), "Your password must be longer than 6 characters.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!this.edtTxtPassword.getEditText().getText().toString().equals(edtTxtPasswordAuth.getEditText().getText().toString())) {
            Toast.makeText(getApplicationContext(), "Your passwords are not the same.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(this.edtTxtPhone.getText().toString().length() < 9) {
            Toast.makeText(getApplicationContext(), "Your phone number must be longer than 9 digits.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!switchAgreeTerms.isChecked()) {
            Toast.makeText(getApplicationContext(), "You must agree to the terms of use in order to register for the application.", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void onRegisterClick(View view) {
        if(isValidInformation()) {
            this.firebaseAuth.createUserWithEmailAndPassword(this.edtTxtEmail.getText().toString(), this.edtTxtPassword.getEditText().getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Register success
                            Log.d("tag1", "createUserWithEmailAndPassword:success");
                            FirebaseDatabase firebaseDatabase;
                            DatabaseReference reference;
                            firebaseDatabase = FirebaseDatabase.getInstance();
                            reference = firebaseDatabase.getReference("users");
                            UserHelperClass helperClass = new UserHelperClass(edtTxtUsername.getText().toString(), edtTxtEmail.getText().toString(), edtTxtPassword.getEditText().getText().toString(), edtTxtPhone.getText().toString());
                            reference.child(edtTxtEmail.getText().toString().replace("@", "").replace(".", "")).setValue(helperClass);
                            Intent intent = new Intent(RegisterActivity.this, UserTypeActivity.class);
                            intent.putExtra("email", edtTxtEmail.getText().toString());
                            startActivity(intent);
                        }
                        else {
                            // If Register fails, display a message to the user.
                            Log.w("tag1", "createUserWithEmailAndPassword:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "The email already exists in the system.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        }
    }
}
