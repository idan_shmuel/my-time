package com.example.mytime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class UserProfileActivity extends AppCompatActivity implements ProfileInterface {

    private TextView txtProfileEmail;
    private EditText edtTxtUsername;
    private EditText edtTxtEmail;
    private EditText edtTxtPhone;
    private TextInputLayout edtTxtPassword;
    private String username;
    private String email;
    private String phoneNumber;
    private String emailWithoutSpecialChars;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        this.txtProfileEmail = findViewById(R.id.txtProfileEmail);
        this.edtTxtUsername = findViewById(R.id.editTextUsername);
        this.edtTxtEmail = findViewById(R.id.editTextEmail);
        this.edtTxtPhone = findViewById(R.id.editTextPhoneNumber);
        this.edtTxtPassword = findViewById(R.id.passwordInputLayout);

        //getting the details from the previous activity.
        Intent intent = getIntent();
        this.email = intent.getStringExtra("email");
        this.emailWithoutSpecialChars = this.email.replace("@", "").replace(".", "");
        this.reference = FirebaseDatabase.getInstance().getReference("users");
        getDataFromDatabase();
    }

    public void getDataFromDatabase() {
        this.reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()) {
                    String usernameFromDB = snapshot.child(emailWithoutSpecialChars).child("username").getValue(String.class);
                    String emailFromDB = snapshot.child(emailWithoutSpecialChars).child("email").getValue(String.class);
                    String phoneNumberFromDB = snapshot.child(emailWithoutSpecialChars).child("phoneNumber").getValue(String.class);
                    String passwordFromDB = snapshot.child(emailWithoutSpecialChars).child("password").getValue(String.class);
                    txtProfileEmail.setText(emailFromDB);
                    edtTxtUsername.setText(usernameFromDB);
                    edtTxtPhone.setText(phoneNumberFromDB);
                    edtTxtPassword.getEditText().setText(passwordFromDB);
                    saveUserData(usernameFromDB, phoneNumberFromDB);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(UserProfileActivity.this, "Fail to get data.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void saveUserData(String usernameFromDB, String phoneNumberFromDB) {
        this.username = usernameFromDB;
        this.phoneNumber = phoneNumberFromDB;
    }

    public void onUpdateClick(View view) {
        if(isDataChanged()) {
            Toast.makeText(UserProfileActivity.this, "Data has been updated", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(UserProfileActivity.this, "Data is same and can not be updated", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isDataChanged() {
        return isUsernameChanged() || isPhoneNumberChanged();
    }

    private boolean isPhoneNumberChanged() {
        if(!this.phoneNumber.equals(this.edtTxtPhone.getText().toString())) {
            this.reference.child(this.emailWithoutSpecialChars).child("phoneNumber").setValue(edtTxtPhone.getText().toString());
            this.phoneNumber = this.edtTxtPhone.getText().toString();
            return true;
        }
        return false;
    }

    private boolean isUsernameChanged() {
        if(!this.username.equals(this.edtTxtUsername.getText().toString())) {
            this.reference.child(this.emailWithoutSpecialChars).child("username").setValue(this.edtTxtUsername.getText().toString());
            this.username = this.edtTxtUsername.getText().toString();
            return true;
        }
        return false;
    }
}