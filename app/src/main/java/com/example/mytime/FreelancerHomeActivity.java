package com.example.mytime;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class FreelancerHomeActivity extends AppCompatActivity {

    private String email;
    private int pickImageRequest;
    private EditText edtTxtFileName;
    private ImageView imgView;
    private ProgressBar progressBar;
    private Uri imgUri;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;
    private StorageTask uploadTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freelancer_home);

        //getting the details from the previous activity.
        Intent intent = getIntent();
        this.email = intent.getStringExtra("email");

        this.pickImageRequest = 1;
        this.edtTxtFileName = findViewById(R.id.edit_text_file_name);
        this.imgView = findViewById(R.id.image_view);
        this.progressBar = findViewById(R.id.progress_bar);

        this.storageReference = FirebaseStorage.getInstance().getReference("images");
        this.databaseReference = FirebaseDatabase.getInstance().getReference("images " + this.email.replace("@", "").replace(".", ""));

        android.app.AlertDialog.Builder screenExplainDialog = new android.app.AlertDialog.Builder(this);
        screenExplainDialog.setMessage("In this screen you can add a new image of your business for everyone to see.").setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        screenExplainDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.userProfile) {
            Intent intent = new Intent(FreelancerHomeActivity.this, UserProfileActivity.class);
            intent.putExtra("email", this.email);
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.freelancerProfile) {
            Intent intent = new Intent(FreelancerHomeActivity.this, FreelancerProfileActivity.class);
            intent.putExtra("email", this.email);
            startActivity(intent);
        }
        else if(item.getItemId() == R.id.logout) {
            createSignOutDialog();
        }
        else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void createSignOutDialog()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Log out of My Time?");
        alertDialog.setPositiveButton("Log Out", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(FreelancerHomeActivity.this, MainActivity.class));
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) { }
        });
        alertDialog.show();
    }

    public void onChooseImgClick(View view) {
        openFileChoose();
    }

    public void onUploadClick(View view) {
        if(this.uploadTask != null && this.uploadTask.isInProgress()) {
            Toast.makeText(FreelancerHomeActivity.this, "Upload in progress", Toast.LENGTH_SHORT).show();
        }
        else {
            uploadFile();
        }
    }

    public void onShowUploadClick(View view) {
        openImagesActivity();
    }

    private void openFileChoose() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, this.pickImageRequest);
    }

    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadFile() {
        if(this.imgUri != null) {
            StorageReference fileReference = this.storageReference.child(System.currentTimeMillis() + "." + getFileExtension(this.imgUri));
            this.uploadTask = fileReference.putFile(this.imgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(0);
                        }
                    }, 500);
                    fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            UploadImage uploadImage = new UploadImage(edtTxtFileName.getText().toString().trim(), uri.toString());
                            String uploadId = databaseReference.push().getKey();
                            databaseReference.child(uploadId).setValue(uploadImage);
                        }
                    });
                    Toast.makeText(FreelancerHomeActivity.this, "Upload successful", Toast.LENGTH_SHORT).show();
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(FreelancerHomeActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            })
            .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                    progressBar.setProgress((int) progress);
                }
            });
        }
        else {
            Toast.makeText(this, "No file selected", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == this.pickImageRequest && resultCode == RESULT_OK && data != null && data.getData() != null) {
            this.imgUri = data.getData();
            Picasso.with(this).load(this.imgUri).into(this.imgView);
        }
    }

    private void openImagesActivity() {
        Intent intent = new Intent(this, ShowImagesActivity.class);
        intent.putExtra("emailWithoutSpecialChars", this.email.replace("@", "").replace(".", ""));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        FirebaseAuth.getInstance().signOut();
        this.finishAffinity();
    }
}