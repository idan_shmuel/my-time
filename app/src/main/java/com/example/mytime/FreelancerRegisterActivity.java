package com.example.mytime;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FreelancerRegisterActivity extends AppCompatActivity {

    private String email;
    private String userType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freelancer_register);
        //getting the phone number from the previous activity.
        Intent intent = getIntent();
        this.email = intent.getStringExtra("email");
        this.userType = intent.getStringExtra("userType");
        //get the spinner from the xml.
        Spinner dropdown = findViewById(R.id.spinnerChooseYourSkill);
        //create a list of items for the spinner.
        String[] items = new String[]{"What's Your Skill?", "barber", "hairdresser", "cosmetician", "other"};
        //create an adapter to describe how the items are displayed, adapters are used in several places in android.
        //There are multiple variations of this, but this is the basic variant.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        //set the spinners adapter to the previously created one.
        dropdown.setAdapter(adapter);
    }

    public void onStartClick(View view) {
        Spinner spinnerSkill = findViewById(R.id.spinnerChooseYourSkill);
        EditText edtTxtBusinessName = findViewById(R.id.editTextBusinessName);
        EditText edtTxtBusinessAddress = findViewById(R.id.editTextBusinessAddress);
        EditText edtTxtBusinessExplanation = findViewById(R.id.editTextBusinessExplanation);

        if(spinnerSkill.getSelectedItem().toString().equals("What's Your Skill?")) {
            Toast.makeText(getApplicationContext(), "You did not enter your skill.", Toast.LENGTH_SHORT).show();
        }
        else if(edtTxtBusinessName.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "You did not enter your business name.", Toast.LENGTH_SHORT).show();
        }
        else if(edtTxtBusinessAddress.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "You did not enter your business address.", Toast.LENGTH_SHORT).show();
        }
        else if(edtTxtBusinessExplanation.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "You did not enter your business explanation.", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getApplicationContext(), "You have successfully registered.", Toast.LENGTH_SHORT).show();
            FirebaseDatabase firebaseDatabase;
            DatabaseReference reference;
            firebaseDatabase = FirebaseDatabase.getInstance();
            reference = firebaseDatabase.getReference("freelancers");
            FreelancerHelperClass helperClass = new FreelancerHelperClass(this.email, spinnerSkill.getSelectedItem().toString(), edtTxtBusinessName.getText().toString(), edtTxtBusinessAddress.getText().toString(), edtTxtBusinessExplanation.getText().toString());
            reference.child(this.email.replace("@", "").replace(".", "")).setValue(helperClass);
            Intent intent = new Intent(FreelancerRegisterActivity.this, FreelancerHomeActivity.class);
            intent.putExtra("email", this.email);
            startActivity(intent);
        }
    }
}