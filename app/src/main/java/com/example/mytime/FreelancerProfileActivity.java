package com.example.mytime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class FreelancerProfileActivity extends AppCompatActivity implements ProfileInterface {

    private TextView txtBusinessName;
    private EditText edtTextBusinessAddress;
    private EditText edtTxtBusinessDescription;
    private String businessAddress;
    private String businessDescription;
    private String email;
    private String emailWithoutSpecialChars;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freelancer_profile);

        this.txtBusinessName = findViewById(R.id.txtBusinessName);
        this.edtTextBusinessAddress = findViewById(R.id.editTextBusinessAddress);
        this.edtTxtBusinessDescription = findViewById(R.id.editTextBusinessExplanation);

        //getting the details from the previous activity.
        Intent intent = getIntent();
        this.email = intent.getStringExtra("email");
        this.emailWithoutSpecialChars = this.email.replace("@", "").replace(".", "");
        this.reference = FirebaseDatabase.getInstance().getReference("freelancers");
        getDataFromDatabase();
    }

    public void getDataFromDatabase() {
        this.reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()) {
                    String businessNameFromDB = snapshot.child(emailWithoutSpecialChars).child("businessName").getValue(String.class);
                    String businessAddressFromDB = snapshot.child(emailWithoutSpecialChars).child("businessAddress").getValue(String.class);
                    String businessDescriptionFromDB = snapshot.child(emailWithoutSpecialChars).child("businessDescription").getValue(String.class);
                    txtBusinessName.setText(businessNameFromDB);
                    edtTextBusinessAddress.setText(businessAddressFromDB);
                    edtTxtBusinessDescription.setText(businessDescriptionFromDB);
                    saveUserData(businessAddressFromDB, businessDescriptionFromDB);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(FreelancerProfileActivity.this, "Fail to get data.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void saveUserData(String businessAddressFromDB, String businessDescriptionFromDB) {
        this.businessAddress = businessAddressFromDB;
        this.businessDescription = businessDescriptionFromDB;
    }

    public void onUpdateClick(View view) {
        if(this.isDataChanged()) {
            Toast.makeText(FreelancerProfileActivity.this, "Data has been updated", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(FreelancerProfileActivity.this, "Data is same and can not be updated", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isDataChanged() {
        return this.isBusinessAddressChanged() || this.isBusinessDescriptionChanged();
    }

    private boolean isBusinessAddressChanged() {
        if(!this.businessAddress.equals(this.edtTextBusinessAddress.getText().toString())) {
            this.reference.child(this.emailWithoutSpecialChars).child("businessAddress").setValue(edtTextBusinessAddress.getText().toString());
            this.businessAddress = this.edtTextBusinessAddress.getText().toString();
            return true;
        }
        return false;
    }

    private boolean isBusinessDescriptionChanged() {
        if(!this.businessDescription.equals(this.edtTxtBusinessDescription.getText().toString())) {
            this.reference.child(this.emailWithoutSpecialChars).child("businessDescription").setValue(this.edtTxtBusinessDescription.getText().toString());
            this.businessDescription = this.edtTxtBusinessDescription.getText().toString();
            return true;
        }
        return false;
    }
}