package com.example.mytime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference usersReference;
    private EditText edtTxtEmail;
    private TextInputLayout edtTxtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.mAuth = FirebaseAuth.getInstance();
        this.usersReference = FirebaseDatabase.getInstance().getReference("users");
        this.edtTxtEmail = findViewById(R.id.editTextEmail);
        this.edtTxtPassword = findViewById(R.id.passwordInputLayout);
    }

    private void checkUser() {
        this.mAuth.signInWithEmailAndPassword(this.edtTxtEmail.getText().toString(), this.edtTxtPassword.getEditText().getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success
                            Log.d("tag1", "signInWithEmail:success");
                            replaceScreenAccordingUserType();
                        }
                        else {
                            // If sign in fails, display a message to the user.
                            Log.w("tag1", "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void replaceScreenAccordingUserType() {
        this.usersReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String userType = snapshot.child(edtTxtEmail.getText().toString().replace("@", "").replace(".", "")).child("userType").getValue(String.class);
                if(userType.equals("client")) {
                    Intent intent = new Intent(LoginActivity.this, ClientHomeActivity.class);
                    intent.putExtra("email", edtTxtEmail.getText().toString());
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(LoginActivity.this, FreelancerHomeActivity.class);
                    intent.putExtra("email", edtTxtEmail.getText().toString());
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(LoginActivity.this, "Fail to get data.", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void onLoginClick(View view) {
        if (this.edtTxtEmail.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "You did not enter your email address.", Toast.LENGTH_SHORT).show();
        }
        else if(this.edtTxtPassword.getEditText().getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "You did not enter your password.", Toast.LENGTH_SHORT).show();
        }
        else {
            checkUser();
        }
    }
}