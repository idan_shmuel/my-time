package com.example.mytime;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ShowImagesActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private DatabaseReference databaseReference;
    private List<UploadImage> uploads;
    private String emailWithoutSpecialChars;
    private ImageAdapter imageAdapter;
    private ProgressBar progressBarCircle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_images);

        //getting the phone number from the previous activity.
        Intent intent = getIntent();
        this.emailWithoutSpecialChars = intent.getStringExtra("emailWithoutSpecialChars");

        this.recyclerView = findViewById(R.id.recyclerView);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        this.progressBarCircle = findViewById(R.id.progress_circle);

        uploads = new ArrayList<>();
        this.databaseReference = FirebaseDatabase.getInstance().getReference("images " + this.emailWithoutSpecialChars);
        this.databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot image : snapshot.getChildren()) {
                    UploadImage uploadImage =  image.getValue(UploadImage.class);
                    uploads.add(uploadImage);
                }
                imageAdapter = new ImageAdapter(ShowImagesActivity.this, uploads);
                recyclerView.setAdapter(imageAdapter);
                progressBarCircle.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(ShowImagesActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                progressBarCircle.setVisibility(View.INVISIBLE);
            }
        });
    }
}