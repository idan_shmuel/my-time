package com.example.mytime;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class SendSMSActivity extends AppCompatActivity {

    private String businessOwnerPhoneNumber;
    private TextView txtBusinessOwnerPhoneNumber;
    private EditText edtTxtMessageContent;
    private BackgroundSoundService backgroundSoundService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_s_m_s);
        this.businessOwnerPhoneNumber = getIntent().getStringExtra("businessOwnerPhoneNumber");
        ActivityCompat.requestPermissions(SendSMSActivity.this, new String[]{Manifest.permission.SEND_SMS, Manifest.permission.READ_SMS}, PackageManager.PERMISSION_GRANTED);

        this.txtBusinessOwnerPhoneNumber = findViewById(R.id.txtBusinessOwnerPhoneNumber);
        this.txtBusinessOwnerPhoneNumber.setText(this.businessOwnerPhoneNumber);
        this.edtTxtMessageContent = findViewById(R.id.edtTxtMessageContent);
    }

    public void onSendMessageClick(View view){
        AlertDialog.Builder sendMessageDialog = new AlertDialog.Builder(this);
        sendMessageDialog.setTitle("you want to send this message to " + this.businessOwnerPhoneNumber + ":");
        sendMessageDialog.setMessage(this.edtTxtMessageContent.getText().toString());
        sendMessageDialog.setPositiveButton("Yes!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startService(new Intent(SendSMSActivity.this, BackgroundSoundService.class));
                SmsManager mySmsManager = SmsManager.getDefault();
                mySmsManager.sendTextMessage(businessOwnerPhoneNumber,null, edtTxtMessageContent.getText().toString(), null, null);
                Toast.makeText(SendSMSActivity.this, "Message sent.", Toast.LENGTH_SHORT).show();
            }
        });
        sendMessageDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) { }
        });
        sendMessageDialog.show();

    }
}
